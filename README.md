# mongodb1
db.createUser({
    user: 'melissa',
    pwd: '123',
    roles: ['readWrite', 'dbAdmin']
});

db.palabras.insert({
   words: 'mopri',
   meaning: 'primo'
})

db.palabras.insert([
    { words: 'xopa', meaning: 'saludos'},
    { words: 'pelao', meaning: 'muchacho'},
    { words: 'vaina', meaning: 'cosa'},
    { words: 'palo', meaning: 'dolar'}
])
db.palabras.find({words: 'vaina'})

db.palabras.update(
    {meaning: 'muchacho'},
    {
        words: 'pelao',
        meaning: 'chico',
    }
)
db.palabras.remove(
     {words: 'xopa'} 
     )
 db.palabras.find(
     {
         $or: [
             {words: 'mopri'},{meaning: 'dolar' }]
     }
 )
 

